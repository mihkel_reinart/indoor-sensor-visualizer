import { Query } from '@/types';

function getCookie(name: string) {
  /* eslint-disable  prefer-template */
  const v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
  return v ? JSON.parse(v[2]) : {};
}

function setCookie(name: string, value: any, hours = 24) {
  const d = new Date();
  d.setTime(d.getTime() + hours * 60 * 60 * 1000);
  document.cookie = name + '=' + JSON.stringify(value) + ';path=/;expires=' + d.toUTCString();
}

function removeCookie(name: string) {
  setCookie(name, null, -1);
}

function getQueryString(q: Query): string {
  if (Object.keys(q).length === 0) {
    return '';
  }

  const queryStringFragments = Object.keys(q).map((key) => {
    const value = q[key];

    if (value === undefined || (Array.isArray(value) && value.length === 0)) {
      return null;
    }

    if (Array.isArray(value)) {
      return (value as Array<string | number | undefined>)
        .map((v) => {
          if (v !== undefined) {
            return `${encodeURIComponent(key)}=${encodeURIComponent(v)}`;
          }
          return null;
        }).join('&');
    }

    return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
  });

  return `?${queryStringFragments.filter((el) => el != null).join('&')}`;
}

function setLocalStorageItem(key: string, value: any, ttl: number): void{
  const item = {
    value,
    expiry: ttl ? Date.now() + ttl : null,
  };

  localStorage.setItem(key, JSON.stringify(item));
}

function getLocalStorageItem(key: string): any {
  const it = localStorage.getItem(key);

  // if the item doesn't exist, return null
  if (!it) return null;

  const item = JSON.parse(it) || {};

  // compare the expiry time of the item with the current time
  if (item.expiry && Date.now() > item.expiry) {
    // If the item is expired, delete the item from storage and return null
    localStorage.removeItem(key);

    return null;
  }

  return item.value;
}

export default {
  getCookie,
  setCookie,
  removeCookie,
  getQueryString,
  getLocalStorageItem,
  setLocalStorageItem,
};
