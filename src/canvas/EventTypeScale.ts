import { ScaleValue } from '@/types';
import DefaultScale from '@/DefaultScales';

const SCALE_SETTINGS_KEY = 'scale_settings';

export default class EventTypeScale {

  private static scaleSettings = JSON.parse(
    localStorage.getItem(SCALE_SETTINGS_KEY) as string,
  ) as { [key: string]: ScaleValue };

  private static cachedColorScales = {} as { [key: string]: { [key: number]: string } };

  static getScaleStart(eventType: string): number {
    const scale = (EventTypeScale.scaleSettings && EventTypeScale.scaleSettings[eventType])
      ? EventTypeScale.scaleSettings[eventType]
      : DefaultScale.defaultScale;

    return scale.start;
  }

  static getScaleEnd(eventType: string): number {
    const scale = (EventTypeScale.scaleSettings && EventTypeScale.scaleSettings[eventType])
      ? EventTypeScale.scaleSettings[eventType]
      : DefaultScale.defaultScale;

    return scale.end;
  }

  static getColor(eventType: string, value: number): string {
    const scale = (EventTypeScale.scaleSettings && EventTypeScale.scaleSettings[eventType])
      ? EventTypeScale.scaleSettings[eventType]
      : DefaultScale.defaultScale;

    const colors = EventTypeScale.getAdvancedColorScale(scale);

    let selectedColor = colors[0];
    /* eslint-disable  no-restricted-syntax */
    const sortedKeys = Object.keys(colors).sort(
      (n1, n2) => parseFloat(n1) - parseFloat(n2),
    );
    for (const key of sortedKeys) {
      const keyNumber = parseFloat(key);
      if (keyNumber > value) {
        break;
      }
      selectedColor = colors[keyNumber];
    }

    return selectedColor;
  }

  private static getAdvancedColorScale(scale: ScaleValue): { [key: number]: string } {
    const colorScaleKey = JSON.stringify(scale) as string;
    if (!EventTypeScale.cachedColorScales[colorScaleKey]) {
      const {
        color0, color25, color50, color75, color100,
      } = scale;

      const color12_5 = EventTypeScale.hexAverage(color0, color25);
      const color37_5 = EventTypeScale.hexAverage(color25, color50);
      const color62_5 = EventTypeScale.hexAverage(color50, color75);
      const color87_5 = EventTypeScale.hexAverage(color75, color100);

      const color6_25 = EventTypeScale.hexAverage(color0, color12_5);
      const color18_75 = EventTypeScale.hexAverage(color12_5, color25);
      const color32_25 = EventTypeScale.hexAverage(color25, color37_5);
      const color43_75 = EventTypeScale.hexAverage(color37_5, color50);
      const color56_25 = EventTypeScale.hexAverage(color50, color62_5);
      const color68_75 = EventTypeScale.hexAverage(color62_5, color75);
      const color81_25 = EventTypeScale.hexAverage(color75, color87_5);
      const color93_75 = EventTypeScale.hexAverage(color87_5, color100);

      EventTypeScale.cachedColorScales[colorScaleKey] = {
        0: color0,
        6.25: color6_25,
        12.5: color12_5,
        18.75: color18_75,
        25: color25,
        32.25: color32_25,
        37.5: color37_5,
        43.75: color43_75,
        50: color50,
        56.25: color56_25,
        62.5: color62_5,
        68.75: color68_75,
        75: color75,
        81.25: color81_25,
        87.5: color87_5,
        93.75: color93_75,
        100: color100,
      };
    }

    return EventTypeScale.cachedColorScales[colorScaleKey];
  }

  private static padToTwo(str: string): string {
    return (str.length < 2) ? `0${str}` : str;
  }

  private static hexAverage(color1: string, color2: string): string {
    if (color1 === color2 || color1.substring(0, 1) !== '#' || color2.substring(0, 1) !== '#') {
      return color1;
    }

    let color = '#';
    for (let i = 0; i < 3; i += 1) {
      const sub1 = color1.substring(1 + 2 * i, 3 + 2 * i);
      const sub2 = color2.substring(1 + 2 * i, 3 + 2 * i);
      const v1 = parseInt(sub1, 16);
      const v2 = parseInt(sub2, 16);
      const v = Math.floor((v1 + v2) / 2);
      const sub = v.toString(16).toUpperCase();
      const padsub = EventTypeScale.padToTwo(sub);
      color += padsub;
    }
    return color;
  }

}
