import { mount } from '@vue/test-utils';
import SideDrawer from '@/components/SideDrawer.vue';
import { nextTick } from 'vue';

describe('side drawer', () => {
  const IS_HIDDEN_CLASS = '-translate-x-80';
  const wrapper = mount(SideDrawer, {
    props: {
      isOpened: false,
    },
    slots: {
      default: 'default slot content',
    },
  });

  it('renders slot contents', async () => {
    const drawerContent = wrapper.find('[data-testid="sideDrawerContent"]');
    expect(drawerContent.text()).toBe('default slot content');
  });

  it('can be opened and closed via prop', async () => {
    expect(wrapper.attributes().class).toContain(IS_HIDDEN_CLASS);

    wrapper.setProps({ isOpened: true });
    await nextTick();
    expect(wrapper.attributes().class).not.toContain(IS_HIDDEN_CLASS);

    wrapper.setProps({ isOpened: false });
    await nextTick();
    expect(wrapper.attributes().class).toContain(IS_HIDDEN_CLASS);
  });

  it('can be opened and closed by clicking on button', async () => {
    const button = wrapper.find('[data-testid="sideDrawerButton"]');
    expect(wrapper.attributes().class).toContain(IS_HIDDEN_CLASS);

    button.trigger('click');
    await nextTick();
    expect(wrapper.attributes().class).not.toContain(IS_HIDDEN_CLASS);

    button.trigger('click');
    await nextTick();
    expect(wrapper.attributes().class).toContain(IS_HIDDEN_CLASS);
  });
});
