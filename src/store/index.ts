import { ActionContext, createStore } from 'vuex';
import {
  ApiArea,
  ApiClient,
  ApiDevice, ApiEvent,
  ApiEventType,
  ApiSite, Area, Device, ScaleValue,
  TimeframeMode,
  TimeSeries,
} from '@/types';
import utils from '@/util/Helpers';
import api from '@/api';
import TimeSeriesBuilder from '@/util/TimeSeriesBuilder';

interface State {
  loadingCounter: number;

  selectedClientId: number;
  selectedSiteId: number;
  selectedAreaId: number;
  selectedStartDateTime: Date;
  selectedEndDateTime: Date;
  selectedTimeframeMode: TimeframeMode;
  selectedEventTypes: string[];

  clients: ApiClient[];
  sites: ApiSite[];
  areas: ApiArea[];
  eventTypes: ApiEventType[];
  devices: ApiDevice[];

  timeSeries: TimeSeries;
}

const state = {
  loadingCounter: 0,

  selectedClientId: utils.getCookie('clientSiteToken').clientId,
  selectedSiteId: utils.getCookie('clientSiteToken').siteId,
  selectedAreaId: 0,
  selectedStartDateTime: new Date(),
  selectedEndDateTime: new Date(),
  selectedTimeframeMode: TimeframeMode.FIXED,
  selectedEventTypes: [],

  clients: [],
  sites: [],
  areas: [],
  eventTypes: [],
  devices: [],

  timeSeries: {},
} as State;

export default createStore({
  state,
  mutations: {
    SET_LOADING_COUNTER(state: State, counter: number): void {
      state.loadingCounter = counter;
    },
    SET_FILTER_OPTIONS(state: State, payload: { [key: string]: any }): void {
      state.clients = payload.clients;
      state.sites = payload.sites;
      state.areas = payload.areas;
      state.eventTypes = payload.eventTypes;
      state.devices = payload.devices;
    },
    SET_FILTER_VALUES(state: State, payload: { [key: string]: any }): void {
      if (payload.clientId) {
        state.selectedClientId = payload.clientId;
      }
      if (payload.siteId) {
        state.selectedSiteId = payload.siteId;
      }
      if (payload.areaId) {
        state.selectedAreaId = payload.areaId;
      }
      if (payload.eventTypes) {
        state.selectedEventTypes = payload.eventTypes;
      }
      if (payload.timeframeMode) {
        state.selectedTimeframeMode = payload.timeframeMode;
      }
      if (payload.startDateTime) {
        state.selectedStartDateTime = payload.startDateTime;
      }
      if (payload.endDateTime) {
        state.selectedEndDateTime = payload.endDateTime;
      }
    },
    SET_TIME_SERIES(state: State, timeseries: TimeSeries): void {
      state.timeSeries = timeseries;
    },
    SET_CLIENTS(state: State, clients: ApiClient[]): void {
      state.clients = clients;
    },
    SET_SITES(state: State, sites: ApiSite[]): void {
      state.sites = sites;
    },
    SET_EVENT_TYPES(state: State, eventTypes: ApiEventType[]): void {
      state.eventTypes = eventTypes;
    },
    SET_CLIENT_ID(state: State, clientId: number): void {
      state.selectedClientId = clientId;
    },
    SET_SITE_ID(state: State, siteId: number): void {
      state.selectedSiteId = siteId;
    },
  },
  actions: {
    fetchFilterOptions,
    fetchEvents,
    fetchTenants,
    fetchEventTypes,
    updateDevice,
    updateArea,
  },
  getters: {
    isLoading: (state: State) => state.loadingCounter > 0,

    clients: (state: State) => state.clients,
    sites: (state: State) => state.sites,
    areas: (state: State) => state.areas,
    eventTypes: (state: State) => state.eventTypes,
    devices: (state: State) => state.devices,

    selectedClientId: (state: State) => state.selectedClientId,
    selectedSiteId: (state: State) => state.selectedSiteId,
    selectedAreaId: (state: State) => state.selectedAreaId,
    selectedEventTypes: (state: State) => state.selectedEventTypes,
    selectedTimeframeMode: (state: State) => state.selectedTimeframeMode,
    selectedStartDateTime: (state: State) => state.selectedStartDateTime,
    selectedEndDateTime: (state: State) => state.selectedEndDateTime,

    timeSeries: (state: State) => state.timeSeries,
  },
});


function incrementLoadingCounter(context: ActionContext<State, Record<string, any>>) {
  const count = context.state.loadingCounter + 1;
  context.commit('SET_LOADING_COUNTER', count);
}

function decrementLoadingCounter(context: ActionContext<State, Record<string, any>>) {
  let count = context.state.loadingCounter - 1;
  if (count < 0) {
    count = 0;
  }
  context.commit('SET_LOADING_COUNTER', count);
}

async function fetchEventTypes(context: ActionContext<State, Record<string, any>>) {
  const eventTypes = await api.fetchEventTypes() as unknown as ApiEventType[];
  context.commit('SET_EVENT_TYPES', eventTypes);
}

async function fetchTenants(context: ActionContext<State, Record<string, any>>) {
  const clients = await api.fetchClients() as unknown as ApiClient[];
  const sites = await api.fetchSites() as unknown as ApiSite[];

  context.commit('SET_CLIENTS', clients);
  context.commit('SET_SITES', sites);
}

async function fetchFilterOptions(context: ActionContext<State, Record<string, any>>) {
  incrementLoadingCounter(context);

  const areas = await api.fetchAreas({
    fk_site: context.state.selectedSiteId,
  }) as unknown as ApiArea[];

  let eventTypes = await api.fetchEventTypes() as unknown as ApiEventType[];
  const SCALE_SETTINGS_KEY = 'scale_settings';
  const scaleSettings = JSON.parse(
    localStorage.getItem(SCALE_SETTINGS_KEY) as string,
  ) as { [key: string]: ScaleValue };

  if (scaleSettings) {
    eventTypes = eventTypes.filter((eventType) => !!scaleSettings[eventType?.id as string]);
  }

  const devices = await api.fetchDevices() as unknown as ApiDevice[];

  context.commit('SET_FILTER_OPTIONS', {
    areas,
    eventTypes,
    devices,
  });

  context.commit('SET_FILTER_VALUES', {
    areaId: areas.length ? areas[0].id : 0,
  });

  decrementLoadingCounter(context);
}

async function fetchEvents(
  context: ActionContext<State, Record<string, any>>,
  payload: { [key: string]: any },
) {
  incrementLoadingCounter(context);
  context.commit('SET_FILTER_VALUES', payload);

  const q = {
    fk_client: state.selectedClientId,
    fk_site: state.selectedSiteId,
    fk_event_type: state.selectedEventTypes,
    start_date: state.selectedStartDateTime.toISOString(),
    end_date: state.selectedEndDateTime.toISOString(),
  } as unknown as ApiEvent;

  let events = [];
  if (state.selectedTimeframeMode === TimeframeMode.LATEST) {
    q.start_date = undefined;
    q.end_date = undefined;
    events = await api.fetchLatestEvents(q);
  } else {
    events = await api.fetchEvents(q);
  }

  let timeseries = TimeSeriesBuilder.makeFromEvents(events, context.getters.devices);
  if (state.selectedTimeframeMode === TimeframeMode.LATEST) {
    const keys = Object.keys(timeseries).map((key: string) => parseFloat(key));
    const latestTimestamp = Math.max(...keys);

    timeseries = {
      [latestTimestamp]: timeseries[latestTimestamp],
    } as TimeSeries;
  }

  context.commit('SET_TIME_SERIES', timeseries);
  decrementLoadingCounter(context);
}

async function updateDevice(
  context: ActionContext<State, Record<string, any>>,
  device: Device,
) {
  incrementLoadingCounter(context);
  const apiDevice = {
    id: device.id,
    name: device.name,
    description: device.description,
    local_latitude: device.localLatitude,
    local_longitude: device.localLongitude,
  } as ApiDevice;

  await api.updateDevice(apiDevice);
  decrementLoadingCounter(context);
}

async function updateArea(
  context: ActionContext<State, Record<string, any>>,
  area: Area,
) {
  incrementLoadingCounter(context);
  const apiArea = {
    id: area.id,
    name: area.name,
    image: area.image,
    image_bounds: area.imageBounds,
  } as ApiArea;

  await api.updateArea(apiArea);
  decrementLoadingCounter(context);
}
