export interface MapCanvasContext {
  zoom: number;
  offsetX: number;
  offsetY: number;
  selectedDevice?: Device | null;
}

export interface BackgroundImage {
  base64image: string;
  bounds: number[][];
}

export interface Device {
  id: number;
  name: string;
  description: string;
  localLatitude: number;
  localLongitude: number;
}

export interface EventType {
  id: string;
  name: string;
  unit: string;
}

export interface Event {
  id: number;
  value: number;
  eventType: string;
}

export interface Area {
  id: number;
  name: string;
  image: string;
  imageBounds?: number[][];
}

export interface DeviceEvents {
  device: Device;
  events: Event[];
}

export interface TimeSeries {
  [timestamp: number]: DeviceEvents[];
}

export interface ScaleValue {
  start: number;
  end: number;
  color0: string;
  color25: string;
  color50: string;
  color75: string;
  color100: string;
}

export interface ScaleSettings {
  [eventType: string]: ScaleValue;
}

export interface Query {
  [key: string]: any;
}

export interface ApiDevice extends Query {
  id?: number;
  name?: string;
  description?: string;
  fk_device_type?: number;
  local_latitude?: number;
  local_longitude?: number;
}

export interface ApiEvent extends Query {
  id?: number;
  value?: number;
  fk_event_type?: string;
  fk_device?: number;
  fk_client?: number;
  fk_site?: number;
  dt_arrival?: string;
  start_date?: string;
  end_date?: string;
}

export interface ApiEventType extends Query {
  id?: string;
  name?: string;
  unit?: string;
}

export interface ApiClient extends Query {
  id?: number;
  name?: string;
}

export interface ApiSite extends Query {
  id?: number;
  name?: string;
  fk_client?: number;
}

export interface ApiArea extends Query {
  id?: number;
  name?: string;
  fk_site?: number;
  image?: string;
  image_bounds?: number[][];
}

export enum TimeframeMode {
  FIXED,
  LATEST,
}
