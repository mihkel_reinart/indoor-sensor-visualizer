import { MapCanvasContext } from '@/types';
import CoordinatesUtil from '@/util/CoordinatesUtil';

let mapContext: MapCanvasContext;
let canvas: HTMLCanvasElement;

let isMouseButtonDown = false;
let prevX = 0;
let prevY = 0;
let currentX = 0;
let currentY = 0;

function mouseDownHandler(e: MouseEvent) {
  isMouseButtonDown = true;
  prevX = e.clientX;
  prevY = e.clientY;
}

function mouseUpHandler() {
  isMouseButtonDown = false;
}

function mouseMoveHandler(e: MouseEvent) {
  if (!isMouseButtonDown) {
    return;
  }

  currentX = e.clientX;
  currentY = e.clientY;

  const { selectedDevice } = mapContext;

  if (selectedDevice) {
    selectedDevice.localLatitude
      += CoordinatesUtil.convertToLatitudeFromCanvasXCoordinate(currentX - prevX, canvas);
    selectedDevice.localLongitude
      += CoordinatesUtil.convertToLongitudeFromCanvasYCoordinate(currentY - prevY, canvas);

  } else {
    mapContext.offsetX += (currentX - prevX);
    mapContext.offsetY += (currentY - prevY);
  }

  prevX = currentX;
  prevY = currentY;
}

function keyPressHandler(e: KeyboardEvent) {
  //if both keyboard and mouse are used at the same time then mouse has priority
  if (isMouseButtonDown) {
    return;
  }

  const arrowMoveDelta = 15;
  if (e.key === 'ArrowUp') {
    mapContext.offsetY -= arrowMoveDelta;

  } else if (e.key === 'ArrowDown') {
    mapContext.offsetY += arrowMoveDelta;

  } else if (e.key === 'ArrowLeft') {
    mapContext.offsetX -= arrowMoveDelta;

  } else if (e.key === 'ArrowRight') {
    mapContext.offsetX += arrowMoveDelta;
  }
}


export default function useMoveOnCanvas(htmlNode: HTMLCanvasElement, mapCtx: MapCanvasContext) {
  mapContext = mapCtx;
  canvas = htmlNode;

  htmlNode.addEventListener('mousemove', mouseMoveHandler);
  htmlNode.addEventListener('mousedown', mouseDownHandler);
  htmlNode.addEventListener('mouseup', mouseUpHandler);
  htmlNode.addEventListener('keyup', keyPressHandler);
}
