import BaseLayer from '@/canvas/BaseLayer';

export default class BackgroundLayer extends BaseLayer {

  private _image!: HTMLImageElement;

  private _bounds!: number[][];

  render(): void {
    if (!this._image) {
      return;
    }

    const x1 = this.convertX(this._bounds[0][0]);
    const y1 = this.convertY(this._bounds[0][1]);
    const x2 = this.convertX(this._bounds[1][0]);
    const y2 = this.convertY(this._bounds[1][1]);

    this.renderCtx.drawImage(
      this._image,
      x1,
      y1,
      (x2 - x1),
      (y2 - y1),
    );
  }

  setImageBounds(bounds: number[][]): void{
    this._bounds = bounds;
  }

  setImageByBase64String(string: string): void{
    const img = new Image();
    img.src = string;
    img.onload = () => {
      this._image = img;
    };
  }
}
