import { DOMWrapper, mount } from '@vue/test-utils';
import EventSequencePlayer from '@/components/EventSequencePlayer.vue';
import { nextTick } from 'vue';

describe('event sequence player', () => {
  const getMountedComponent = () => mount(EventSequencePlayer, {
    props: {
      timestamps: [
        1612163198,
        1613163198,
        1614163198,
      ],
    },
  });

  it('renders buttons for all timestamps', async () => {
    const wrapper = getMountedComponent();
    expect(wrapper.findAll('[data-testid="timestampButton"]').length).toBe(3);
  });

  it('emits timestamp-changed when timestamp changes', async () => {
    const wrapper = getMountedComponent();
    const playButton = wrapper.find('[data-testid="playButton"]');
    expect(playButton.exists()).toBeTruthy();

    await playButton.trigger('click');
    await nextTick();

    const emitted = wrapper.emitted('timestamp-changed') as unknown[];
    expect(emitted).toBeTruthy();
    expect(emitted.length).toBe(2);
    //first event is emitted when mounted. second event when we press play button
    expect(emitted).toStrictEqual([[1612163198], [1613163198]]);
  });

  it('rewinds back to beginning when player reaches end and player is started again', async () => {
    const wrapper = getMountedComponent();
    const playButton = wrapper.find('[data-testid="playButton"]');
    const timestampButtons = wrapper.findAll('[data-testid="timestampButton"]');
    const lastButton = timestampButtons.slice(-1).pop() as DOMWrapper<Element>;

    expect(playButton.exists()).toBeTruthy();
    expect(lastButton?.exists()).toBeTruthy();

    await lastButton.trigger('click');
    await nextTick();

    await playButton.trigger('click');
    await nextTick();

    const emitted = wrapper.emitted('timestamp-changed') as unknown[];
    expect(emitted).toBeTruthy();
    expect(emitted.length).toBe(3);

    //first event is emitted when mounted. second event when we rewind to last event.
    //3rd when we press play again and rewind back to beginning
    expect(emitted).toStrictEqual([[1612163198], [1614163198], [1612163198]]);
  });

  it('renders text about current index and total number of timestamps', async () => {
    const wrapper = getMountedComponent();
    const playButton = wrapper.find('[data-testid="playButton"]');
    const currentOfTotal = wrapper.find('[data-testid="currentOfTotal"]');

    expect(playButton.exists()).toBeTruthy();
    expect(currentOfTotal.text()).toBe('1/3');

    await playButton.trigger('click');
    await nextTick();

    expect(currentOfTotal.text()).toBe('2/3');
  });

  it('renders pretty text about currently displayed timestamp', async () => {
    // text is made prettier using toLocaleString;
    // override to enforce en-GB locale and UTC timezone for tests
    const { toLocaleString } = Date.prototype;
    // eslint-disable-next-line no-extend-native
    Date.prototype.toLocaleString = function () {
      return toLocaleString.call(this, 'en-GB', { timeZone: 'UTC' });
    };

    const wrapper = getMountedComponent();
    const playButton = wrapper.find('[data-testid="playButton"]');
    const currentTimestampTextContainer = wrapper.find('[data-testid="currentTimestamp"]');

    await nextTick(); //text gets rendered after 1 tick

    expect(playButton.exists()).toBeTruthy();
    expect(currentTimestampTextContainer.text()).toBe('01/02/2021, 07:06:38');//1612163198

    await playButton.trigger('click');
    await nextTick();

    expect(currentTimestampTextContainer.text()).toBe('12/02/2021, 20:53:18');//1613163198
  });

  it('does not display buttons if only one or no timestamps at all', async () => {
    const wrapper1Timestamp = mount(EventSequencePlayer, {
      props: {
        timestamps: [
          1612163198,
        ],
      },
    });

    const buttons2 = wrapper1Timestamp.findAll('[data-testid="timestampButton"]');
    expect(buttons2.length).toBe(0);

    const wrapperNoTimestamps = mount(EventSequencePlayer, {
      props: {
        timestamps: [],
      },
    });

    const buttons1 = wrapperNoTimestamps.findAll('[data-testid="timestampButton"]');
    expect(buttons1.length).toBe(0);
  });

});
