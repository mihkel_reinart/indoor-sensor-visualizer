import SwitchButton from '@/components/SwitchButton.vue';
import { nextTick } from 'vue';
import { mount } from '@vue/test-utils';

describe('switch button', () => {
  const wrapper = mount(SwitchButton, {
    slots: {
      option0: 'option 0 content',
      option1: 'option 1 content',
    },
    props: {
      selected: 0,
    },
  });

  it('renders slot contents', () => {
    const option0Label = wrapper.find('[data-testid="option0"]');
    const option1Label = wrapper.find('[data-testid="option1"]');
    expect(option0Label.text()).toBe('option 0 content');
    expect(option1Label.text()).toBe('option 1 content');
  });

  it('emits value when toggled', async () => {
    const toggleButton = wrapper.find('[data-testid="toggleButton"]');

    //switch initial value 0 is turned into 1
    toggleButton.trigger('click');
    await nextTick();

    //switch value 1 is turned into 0
    toggleButton.trigger('click');
    await nextTick();

    //switch value 0 is turned into 1 again
    toggleButton.trigger('click');
    await nextTick();

    const emitted = wrapper.emitted('switch-changed') as unknown[];
    expect(emitted).toBeTruthy();
    expect(emitted.length).toBe(3);
    expect(emitted).toStrictEqual([[1], [0], [1]]);
  });
});
