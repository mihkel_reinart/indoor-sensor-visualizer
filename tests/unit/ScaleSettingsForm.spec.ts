import { mount } from '@vue/test-utils';
import ScaleSettingsForm from '@/components/ScaleSettingsForm.vue';
import { ScaleSettings, EventType } from '@/types';
import { nextTick } from 'vue';

describe('scale settings form', () => {
  const getMock = () => mount(ScaleSettingsForm, {
    props: {
      eventTypes: [
        {
          id: 'movement',
          name: 'MovementType',
          unit: '',
        } as EventType,
      ],
      scaleSettings: {
        movement: {
          start: 20,
          end: 80,
          color0: '#aabbcc',
          color25: '#bbccdd',
          color50: '#ccddee',
          color75: '#ddeeff',
          color100: '#eeffff',
        },
      } as ScaleSettings,
    },
  });

  it('renders form inputs for scale attributes', () => {
    const wrapper = getMock();

    expect(wrapper.findAll('input[type="number"]').length).toBe(2);
    expect(wrapper.findAll('input[type="color"]').length).toBe(5);
  });

  it('emits data when form is submitted', async () => {
    const wrapper = getMock();
    const form = wrapper.find('form');

    await form.trigger('submit');
    await nextTick();

    const emitted = wrapper.emitted('form-submitted') as unknown[];
    expect(emitted).toBeTruthy();
    expect(emitted.length).toBe(1);
    expect(emitted).toStrictEqual([[{
      movement: {
        color0: '#aabbcc',
        color25: '#bbccdd',
        color50: '#ccddee',
        color75: '#ddeeff',
        color100: '#eeffff',
        end: 80,
        start: 20,
      },
    }]]);
  });
});
