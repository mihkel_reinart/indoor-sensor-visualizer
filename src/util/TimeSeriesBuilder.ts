import {
  TimeSeries, ApiEvent, ApiDevice, Event, Device, DeviceEvents,
} from '@/types';

export default class TimeSeriesBuilder {


  /**
   * Builds a TimeSeries object based on events and devices.
   * It uses the event arrival date as time series keys.
   *
   * Each series step after the first one builds upon the previous one.
   * The values are hold per device and per event type.
   *
   * Example 1:
   * Event 1 has device A and event type X @ time 1.
   * Event 2 has device B and event type X @ time 2.
   * Then series step @ time 1 has info about event 1,
   * and series step @ time 2 has info about both event 1 and event 2 (since different devices).
   *
   * Example 2:
   * Event 1 has device A and event type X @ time 1.
   * Event 2 has device A and event type X @ time 2.
   * Then series step @ time 1 has info about event 1,
   * and series @ time 2 has only info about event 2 (since same devices and event types).
   *
   * */
  static makeFromEvents(events: ApiEvent[], devices: ApiDevice[]): TimeSeries {
    if (!events || !devices) {
      return {};
    }

    let prevTimestamp = 0;
    const states = new Map();
    events.forEach((apiEvent: ApiEvent) => {
      const timestamp = new Date(apiEvent.dt_arrival as string).getTime() / 1000;
      const deviceId = apiEvent.fk_device;
      const eventType = apiEvent.fk_event_type;
      const eventId = apiEvent.id;

      const timestampDevices = states.get(timestamp)
        || new Map(states.get(prevTimestamp))
        || new Map();

      const deviceEvents = new Map(timestampDevices.get(deviceId))
        || new Map();

      deviceEvents.set(eventType, eventId);
      timestampDevices.set(deviceId, deviceEvents);
      states.set(timestamp, timestampDevices);

      if (prevTimestamp !== timestamp) {
        prevTimestamp = timestamp;
      }
    });

    const timeSeries = {} as TimeSeries;
    states.forEach((
      devicesEventTypes: Map<number, Map<string, number>>,
      timestamp: number,
    ) => {
      timeSeries[timestamp] = Array.from(devicesEventTypes).map(([deviceId, eventTypes]) => {
        const evs = Array.from(eventTypes).map(([eventType, eventId]) => {
          const aEvent = events.find((ev: ApiEvent) => ev.id === eventId) || {} as ApiEvent;

          return {
            id: aEvent.id,
            value: aEvent.value,
            eventType: aEvent.fk_event_type,
          } as Event;
        });

        const aDev = devices.find((d: ApiDevice) => d.id === deviceId) || {} as ApiDevice;

        return {
          device: {
            id: aDev.id,
            description: aDev.description,
            name: aDev.name,
            localLatitude: aDev.local_latitude || 0,
            localLongitude: aDev.local_longitude || 0,
          } as Device,
          events: evs,
        } as DeviceEvents;
      });
    });

    return timeSeries;
  }

}
