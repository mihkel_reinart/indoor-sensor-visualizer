import { MapCanvasContext, Device } from '@/types';
import CoordinatesUtil from '@/util/CoordinatesUtil';

let mapContext: MapCanvasContext;
let canvas: HTMLCanvasElement;
let devices: Device[];
let emitter: Function;

function getHoveredDevice(e: MouseEvent): Device | null {
  const mouseX = e.offsetX;
  const mouseY = e.offsetY;
  const radius = 20;
  for (let i = 0; i < devices.length; i += 1) {
    const item: Device = devices[i];
    const x = CoordinatesUtil.convertToCanvasXCoordinate(item.localLatitude, canvas, mapContext);
    const y = CoordinatesUtil.convertToCanvasYCoordinate(item.localLongitude, canvas, mapContext);

    if (mouseX > x - radius
      && mouseY > y - radius
      && mouseX < x + radius
      && mouseY < y + radius) {
      return item;
    }
  }
  return null;
}

function handleMouseMoveEvent(e: MouseEvent) {
  const hoveredItem = getHoveredDevice(e);
  if (hoveredItem) {
    document.body.style.cursor = 'pointer';
  } else {
    document.body.style.cursor = 'default';
  }
}

function handleMouseDownEvent(e: MouseEvent) {
  const hoveredItem = getHoveredDevice(e);
  if (hoveredItem) {
    mapContext.selectedDevice = hoveredItem;
  }
}

function handleMouseUpEvent() {
  if (mapContext.selectedDevice) {
    emitter('device-moved', mapContext.selectedDevice);
    mapContext.selectedDevice = null;
  }
}

export default function useSelectOnCanvas(
  htmlNode: HTMLCanvasElement,
  mapCtx: MapCanvasContext,
  listOfDevices: Device[],
  emit: Function,
) {
  mapContext = mapCtx;
  canvas = htmlNode;
  devices = listOfDevices;
  emitter = emit;

  canvas.addEventListener('mousemove', handleMouseMoveEvent);
  canvas.addEventListener('mousedown', handleMouseDownEvent);
  canvas.addEventListener('mouseup', handleMouseUpEvent);
}
