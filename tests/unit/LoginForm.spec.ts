import LoginForm from '@/components/LoginForm.vue';
import { nextTick } from 'vue';
import { mount } from '@vue/test-utils';

describe('login form', () => {
  it('renders form with username, password fields and submit button', () => {
    const wrapper = mount(LoginForm);
    const form = wrapper.find('form');
    const usernameInput = wrapper.find('[data-testid="usernameInput"]');
    const passwordInput = wrapper.find('[data-testid="passwordInput"]');
    const submitButton = wrapper.find('[data-testid="formSubmitButton"]');

    expect(form.exists()).toBeTruthy();
    expect(usernameInput.exists()).toBeTruthy();
    expect(passwordInput.exists()).toBeTruthy();
    expect(submitButton.exists()).toBeTruthy();
  });

  it('emits form-submitted with data when submitted', async () => {
    const wrapper = mount(LoginForm);
    const form = wrapper.find('form');
    const usernameInput = wrapper.find('[data-testid="usernameInput"]');
    const passwordInput = wrapper.find('[data-testid="passwordInput"]');

    usernameInput.setValue('test-username');
    passwordInput.setValue('test-password');
    await form.trigger('submit');
    await nextTick();

    const emitted = wrapper.emitted('form-submitted') as unknown[];
    expect(emitted).toBeTruthy();
    expect(emitted.length).toBe(1);
    expect(emitted).toStrictEqual([[{ username: 'test-username', password: 'test-password' }]]);
  });

  it('renders error message if passed as prop', () => {
    const wrapper = mount(LoginForm, {
      props: {
        errorText: 'something bad happened',
      },
    });

    const errorContainer = wrapper.find('[data-testid="errorTextContainer"]');
    expect(errorContainer.exists()).toBeTruthy();
    expect(errorContainer.text()).toBe('something bad happened');
  });

  it('disables inputs and button when in-loading prop is truthy', () => {
    const form1 = mount(LoginForm, {
      props: {
        isLoading: false,
      },
    });
    const usernameInput1 = form1.find('[data-testid="usernameInput"]');
    const passwordInput1 = form1.find('[data-testid="passwordInput"]');
    const submitButton1 = form1.find('[data-testid="formSubmitButton"]');

    expect(usernameInput1.attributes('disabled')).toBeUndefined();
    expect(passwordInput1.attributes('disabled')).toBeUndefined();
    expect(submitButton1.attributes('disabled')).toBeUndefined();

    const form2 = mount(LoginForm, {
      props: {
        isLoading: true,
      },
    });
    const usernameInput2 = form2.find('[data-testid="usernameInput"]');
    const passwordInput2 = form2.find('[data-testid="passwordInput"]');
    const submitButton2 = form2.find('[data-testid="formSubmitButton"]');

    expect(usernameInput2.attributes('disabled')).not.toBeUndefined();
    expect(passwordInput2.attributes('disabled')).not.toBeUndefined();
    expect(submitButton2.attributes('disabled')).not.toBeUndefined();
  });

});
