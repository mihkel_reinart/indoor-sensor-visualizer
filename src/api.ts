import axios from 'axios';
import {
  ApiArea, ApiClient, ApiDevice, ApiEvent, ApiEventType, ApiSite, Query,
} from '@/types';
import Helpers from '@/util/Helpers';

const baseURL = process.env.VUE_APP_API_URL;

const authURL = `${baseURL}/token`;
const clientsURL = `${baseURL}/clients`;
const sitesURL = `${baseURL}/sites`;
const areasURL = `${baseURL}/areas`;
const devicesURL = `${baseURL}/devices`;
const eventTypesURL = `${baseURL}/reference/eventtypes`;
const eventsURL = `${baseURL}/events`;
const latestURL = `${baseURL}/events/latest`;

function initAuthHeaderOrFail() {
  let tokenData = Helpers.getCookie('clientSiteToken');
  if (Object.keys(tokenData).length === 0) {
    tokenData = Helpers.getCookie('initialToken');
  }
  const { token } = tokenData;

  if (!token || !token.length) {
    throw new Error('Request not available without token');
  }
  axios.defaults.headers.common.Authorization = `Basic ${btoa(`${token}:`)}`;
}

async function fetchData(url: string, query?: Query): Promise<any> {
  initAuthHeaderOrFail();

  const urlWithParams = `${url}${Helpers.getQueryString(query || {})}`;

  const resp = await axios.get(urlWithParams);
  if (resp && resp.status === 200) {
    return resp.data.data;
  }

  return null;
}

async function fetchDataWithCache(url: string, query?: Query): Promise<any> {
  initAuthHeaderOrFail();

  const cachedResponse = Helpers.getLocalStorageItem(url);

  if (cachedResponse) {
    return cachedResponse;
  }

  const urlWithParams = `${url}${Helpers.getQueryString(query || {})}`;
  const resp = await axios.get(urlWithParams);
  if (resp && resp.status === 200) {
    const responseData = resp.data.data;
    Helpers.setLocalStorageItem(url, responseData, 60 * 60 * 1000); //cache for 1 hour
    return responseData;
  }

  return null;
}

async function authenticateUser(authPayload: { [key: string]: string }): Promise<void> {
  const { username, password } = authPayload;

  const resp = await axios.get(authURL, {
    headers: {
      Authorization: `Basic ${btoa(`${username}:${password}`)}`,
    },
  });

  if (resp.status !== 200 || !resp.data.token) {
    return;
  }

  await Helpers.setCookie('initialToken', {
    id: resp.data.id,
    username: resp.data.username,
    token: resp.data.token,
  });
}

async function authenticateTenant(authPayload: { [key: string]: number }): Promise<void> {
  const { clientId, siteId } = authPayload;

  initAuthHeaderOrFail();

  const resp = await axios.get(`${authURL}/client/${clientId}/site/${siteId}/`);

  if (resp.status !== 200 || !resp.data.token) {
    return;
  }

  await Helpers.setCookie('clientSiteToken', {
    id: resp.data.id,
    username: resp.data.username,
    token: resp.data.token,
    clientId,
    siteId,
  });
}

async function invalidateUserTokens(): Promise<void> {
  Helpers.removeCookie('initialToken');
  Helpers.removeCookie('clientSiteToken');
}

const fetchClients = async (): Promise<ApiClient[]> => fetchDataWithCache(clientsURL);

const fetchSites = async (): Promise<ApiSite[]> => fetchDataWithCache(sitesURL);

const fetchAreas = async (
  q?: ApiArea): Promise<ApiArea[]> => fetchDataWithCache(areasURL, q);

const fetchDevices = async (
  q?: ApiDevice): Promise<ApiDevice[]> => fetchDataWithCache(devicesURL, q);

const fetchEventTypes = async (): Promise<ApiEventType[]> => fetchDataWithCache(eventTypesURL);

const fetchEvents = async (
  q?: ApiEvent): Promise<ApiEvent[]> => fetchData(eventsURL, q);

const fetchLatestEvents = async (
  q?: ApiEvent): Promise<ApiEvent[]> => fetchData(latestURL, q);

const updateDevice = async (dev: ApiDevice) => {
  initAuthHeaderOrFail();

  await axios.put(`${devicesURL}/${dev.id}`, {
    local_latitude: dev.local_latitude,
    local_longitude: dev.local_longitude,
  });

  Helpers.setLocalStorageItem(devicesURL, {}, 1);//invalidate cached result
};

const updateArea = async (area: ApiArea) => {
  initAuthHeaderOrFail();

  await axios.put(`${areasURL}/${area.id}`, {
    image: area.image,
    image_bounds: area.image_bounds,
  });

  Helpers.setLocalStorageItem(areasURL, {}, 1);//invalidate cached result
};

export default {
  authenticateUser,
  authenticateTenant,
  invalidateUserTokens,
  fetchClients,
  fetchSites,
  fetchAreas,
  fetchDevices,
  fetchEventTypes,
  fetchEvents,
  fetchLatestEvents,
  updateDevice,
  updateArea,
};
