import { MapCanvasContext } from '@/types';
import { zoomMultiplierX, zoomMultiplierY } from '@/composables/ZoomOnCanvas';

function convertToCanvasXCoordinate(
  x: number,
  canvas: HTMLCanvasElement,
  mapContext: MapCanvasContext,
): number {
  const pixelRatio = window.devicePixelRatio;
  const canvasWidth = (canvas.width / pixelRatio);
  return (canvasWidth / 2 + mapContext.offsetX) - (zoomMultiplierX() * x);
}

function convertToCanvasYCoordinate(
  y: number,
  canvas: HTMLCanvasElement,
  mapContext: MapCanvasContext,
): number {
  const pixelRatio = window.devicePixelRatio;
  const canvasHeight = (canvas.height / pixelRatio);
  return (canvasHeight / 2 + mapContext.offsetY) - (zoomMultiplierY() * y);
}

function convertToLatitudeFromCanvasXCoordinate(
  x: number,
  canvas: HTMLCanvasElement,
): number {
  const pixelRatio = window.devicePixelRatio;
  const canvasWidth = (canvas.width / pixelRatio);
  const zoom = zoomMultiplierX();
  const halfCanvasWidth = canvasWidth / 2;

  return ((halfCanvasWidth - x) / zoom) - (halfCanvasWidth / zoom);
}

function convertToLongitudeFromCanvasYCoordinate(
  y: number,
  canvas: HTMLCanvasElement,
): number {
  const pixelRatio = window.devicePixelRatio;
  const canvasHeight = (canvas.height / pixelRatio);
  const zoom = zoomMultiplierY();
  const halfCanvasHeight = canvasHeight / 2;

  return ((halfCanvasHeight - y) / zoom) - (halfCanvasHeight / zoom);
}

export default {
  convertToCanvasXCoordinate,
  convertToCanvasYCoordinate,
  convertToLatitudeFromCanvasXCoordinate,
  convertToLongitudeFromCanvasYCoordinate,
};
