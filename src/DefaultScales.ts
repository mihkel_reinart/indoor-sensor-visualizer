import { ScaleSettings, ScaleValue } from '@/types';

/*
* THESE SCALES NEED FINE TUNING;
* NONE OF THESE VALUES HAVE BEEN THOUGHT THROUGH MORE
* THAN TO MAKE DIFFERENT EVENT TYPES HAVE DIFFERENT COLORS
*
* */

const defaultScale = {
  start: 0,
  end: 1000,
  color0: '#3a6995',
  color25: '#5396d5',
  color50: '#dddddd',
  color75: '#f2a161',
  color100: '#aa7142',
} as ScaleValue;

const temperatureScale = {
  start: 18,
  end: 26,
  color0: '#8dd0fa',
  color25: '#cbfdf5',
  color50: '#f2febe',
  color75: '#f7c96c',
  color100: '#ee6a2e',
} as ScaleValue;

const movementScale = {
  start: 0,
  end: 100,
  color0: '#d5f0de',
  color25: '#82ca98',
  color50: '#67c17c',
  color75: '#56a460',
  color100: '#3d754b',
};

const humidityScale = {
  start: 0,
  end: 1000,
  color0: '#e2ded5',
  color25: '#a9a1c1',
  color50: '#9185b9',
  color75: '#5c54ab',
  color100: '#002196',
};

const co2Scale = {
  start: 300,
  end: 1000,
  color0: '#eee3d4',
  color25: '#caaabc',
  color50: '#a672a4',
  color75: '#833a8c',
  color100: '#711680',
};

export default {
  defaultScale,
  ambient_temperature_C: temperatureScale,
  dt_movement_count: movementScale,
  dt_relative_humidity_pct: humidityScale,
  dt_co2_ppm: co2Scale,
} as ScaleSettings;
