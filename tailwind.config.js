/* eslint @typescript-eslint/no-var-requires: "off" */
const colors = require('tailwindcss/colors');

module.exports = {
  purge: [
    './src/**/*.vue',
  ],
  theme: {
    colors: {
      ...colors,
      gray: colors.blueGray,
    },
    extend: {},
  },
  variants: {},
  plugins: [],
};
