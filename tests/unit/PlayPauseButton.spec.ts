import PlayPauseButton from '@/components/PlayPauseButton.vue';
import { nextTick } from 'vue';
import { mount } from '@vue/test-utils';

describe('play-pause button', () => {
  const wrapper = mount(PlayPauseButton);

  it('emits play event when clicked play button', async () => {
    wrapper.setProps({
      modelValue: false, //is play button hidden
    });

    await nextTick();
    expect(wrapper.find('[data-testid="playButton"]').exists()).toBeTruthy();

    wrapper.trigger('click');
    await nextTick();
    const emitted = wrapper.emitted('play-button-clicked') as unknown[];
    expect(emitted).toBeTruthy();
    expect(emitted.length).toBe(1);
  });

  it('emits pause event when clicked pause button', async () => {
    wrapper.setProps({
      modelValue: true, //is play button hidden
    });

    await nextTick();
    expect(wrapper.find('[data-testid="pauseButton"]').exists()).toBeTruthy();

    wrapper.trigger('click');
    await nextTick();
    const emitted = wrapper.emitted('pause-button-clicked') as unknown[];
    expect(emitted).toBeTruthy();
    expect(emitted.length).toBe(1);
  });
});
