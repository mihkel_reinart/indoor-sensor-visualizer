import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import Dashboard from '@/views/Dashboard.vue';
import Login from '@/views/Login.vue';
import Settings from '@/views/Settings.vue';
import Tenants from '@/views/Tenants.vue';
import utils from '@/util/Helpers';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/tenants',
    name: 'Tenants',
    component: Tenants,
    meta: {
      requiresAuth: true,
    },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const isLoggedIn = utils.getCookie('initialToken').token?.length > 0;

  if (!isLoggedIn && to.matched.some((r) => r.meta.requiresAuth)) {
    next({ name: 'Login' });

  } else if (to.name === undefined) {
    next({ name: 'Dashboard' });

  } else {
    next();

  }
});

export default router;
