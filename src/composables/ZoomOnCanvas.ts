import { MapCanvasContext } from '@/types';

const maxZoom = 10;
const minZoom = 1;

let mapContext: MapCanvasContext;
let canvas: HTMLCanvasElement;

function handleKeysPressed(e: KeyboardEvent) {
  if (e.key === '-') {
    zoomOut();

  } else if (e.key === '+') {
    zoomIn();
  }
}

export function zoomMultiplierX(): number {
  const { devicePixelRatio } = window;
  const canvasWidth = ((canvas ? canvas.width : 0) / devicePixelRatio) as number;
  return (canvasWidth / (mapContext.zoom * 100));
}

export function zoomMultiplierY(): number {
  const { devicePixelRatio } = window;
  const canvasHeight = ((canvas ? canvas.height : 0) / devicePixelRatio) as number;
  return (canvasHeight / (mapContext.zoom * 100));
}

export function zoomIn() {
  mapContext.zoom = Math.max(minZoom, mapContext.zoom - 1);
}

export function zoomOut() {
  mapContext.zoom = Math.min(maxZoom, mapContext.zoom + 1);
}

export default function useZoomOnCanvas(htmlNode: HTMLCanvasElement, mapCtx: MapCanvasContext) {
  mapContext = mapCtx;
  canvas = htmlNode;

  canvas.addEventListener('keyup', handleKeysPressed);
}
