import TimeSeriesBuilder from '@/util/TimeSeriesBuilder';
import {
  ApiDevice, ApiEvent, Device, Event, DeviceEvents,
} from '@/types';

describe('timeseries builder', () => {
  const getTimestampNumber = (ts: string): number => new Date(ts as string).getTime() / 1000;

  it('builds empty series when no events and not devices passed', () => {
    const emptySeries = TimeSeriesBuilder.makeFromEvents([], []);
    expect(emptySeries).toStrictEqual({});
  });

  it('builds series valid series if 1 event passed', () => {
    const apiEvent = {
      id: 1,
      value: 100,
      fk_event_type: 'temperature',
      fk_device: 666,
      dt_arrival: '2021-03-13T16:17:11.157000+00:00',
    } as ApiEvent;

    const apiDevice = {
      id: 666,
      name: 'test-device-1',
      description: 'test-device-1-description',
    } as ApiDevice;

    const series = TimeSeriesBuilder.makeFromEvents([apiEvent], [apiDevice]);

    const event = {
      id: apiEvent.id,
      value: apiEvent.value,
      eventType: apiEvent.fk_event_type,
    } as Event;

    const device = {
      id: apiDevice.id,
      name: apiDevice.name,
      description: apiDevice.description,
      localLatitude: 0,
      localLongitude: 0,
    } as Device;

    expect(series).toStrictEqual({
      [getTimestampNumber(apiEvent.dt_arrival as string)]: [
        {
          device,
          events: [event],
        } as DeviceEvents,
      ],
    });
  });

  it('builds series where previous values are passed on', () => {
    const time1 = '2021-03-13T00:00:00.000000+00:00';
    const time2 = '2021-03-14T00:00:00.000000+00:00';
    const time3 = '2021-03-15T00:00:00.000000+00:00';

    const apiEvent1 = {
      id: 1,
      value: 100,
      fk_event_type: 'temperature',
      fk_device: 666,
      dt_arrival: time1,
    } as ApiEvent;

    const apiEvent2 = {
      id: 2,
      value: 200,
      fk_event_type: 'temperature',
      fk_device: 777,
      dt_arrival: time2,
    } as ApiEvent;

    const apiEvent3 = {
      id: 3,
      value: 300,
      fk_event_type: 'temperature',
      fk_device: 666,
      dt_arrival: time3,
    } as ApiEvent;

    const apiDevice1 = {
      id: 666,
      name: 'test-device-1',
      description: 'test-device-1-description',
    } as ApiDevice;

    const apiDevice2 = {
      id: 777,
      name: 'test-device-2',
      description: 'test-device-2-description',
    } as ApiDevice;

    const series = TimeSeriesBuilder.makeFromEvents(
      [apiEvent1, apiEvent2, apiEvent3],
      [apiDevice1, apiDevice2],
    );

    const event1 = {
      id: apiEvent1.id,
      value: apiEvent1.value,
      eventType: apiEvent1.fk_event_type,
    } as Event;

    const event2 = {
      id: apiEvent2.id,
      value: apiEvent2.value,
      eventType: apiEvent2.fk_event_type,
    } as Event;

    const event3 = {
      id: apiEvent3.id,
      value: apiEvent3.value,
      eventType: apiEvent3.fk_event_type,
    } as Event;

    const device1 = {
      id: apiDevice1.id,
      name: apiDevice1.name,
      description: apiDevice1.description,
      localLatitude: 0,
      localLongitude: 0,
    } as Device;

    const device2 = {
      id: apiDevice2.id,
      name: apiDevice2.name,
      description: apiDevice2.description,
      localLatitude: 0,
      localLongitude: 0,
    } as Device;

    expect(series).toStrictEqual({
      [getTimestampNumber(time1)]: [
        {
          device: device1,
          events: [event1],
        } as DeviceEvents,
      ],
      [getTimestampNumber(time2)]: [
        {
          device: device1,
          events: [event1],
        } as DeviceEvents,
        {
          device: device2,
          events: [event2],
        } as DeviceEvents,
      ],
      [getTimestampNumber(time3)]: [
        {
          device: device1,
          events: [event3],
        } as DeviceEvents,
        {
          device: device2,
          events: [event2],
        } as DeviceEvents,
      ],
    });
  });

  it('builds series where only events with same event type change between steps', () => {
    const time1 = '2021-03-13T00:00:00.000000+00:00';
    const time2 = '2021-03-14T00:00:00.000000+00:00';
    const time3 = '2021-03-15T00:00:00.000000+00:00';

    const apiEvent1 = {
      id: 1,
      value: 100,
      fk_event_type: 'temperature',
      fk_device: 666,
      dt_arrival: time1,
    } as ApiEvent;

    const apiEvent2 = {
      id: 2,
      value: 200,
      fk_event_type: 'movement',
      fk_device: 666,
      dt_arrival: time2,
    } as ApiEvent;

    const apiEvent3 = {
      id: 3,
      value: 300,
      fk_event_type: 'temperature',
      fk_device: 666,
      dt_arrival: time3,
    } as ApiEvent;

    const apiDevice1 = {
      id: 666,
      name: 'test-device-1',
      description: 'test-device-1-description',
    } as ApiDevice;

    const series = TimeSeriesBuilder.makeFromEvents(
      [apiEvent1, apiEvent2, apiEvent3],
      [apiDevice1],
    );

    const event1 = {
      id: apiEvent1.id,
      value: apiEvent1.value,
      eventType: apiEvent1.fk_event_type,
    } as Event;

    const event2 = {
      id: apiEvent2.id,
      value: apiEvent2.value,
      eventType: apiEvent2.fk_event_type,
    } as Event;

    const event3 = {
      id: apiEvent3.id,
      value: apiEvent3.value,
      eventType: apiEvent3.fk_event_type,
    } as Event;

    const device1 = {
      id: apiDevice1.id,
      name: apiDevice1.name,
      description: apiDevice1.description,
      localLatitude: 0,
      localLongitude: 0,
    } as Device;

    expect(series).toStrictEqual({
      [getTimestampNumber(time1)]: [
        {
          device: device1,
          events: [event1],
        } as DeviceEvents,
      ],
      [getTimestampNumber(time2)]: [
        {
          device: device1,
          events: [event1, event2],
        } as DeviceEvents,
      ],
      [getTimestampNumber(time3)]: [
        {
          device: device1,
          events: [event3, event2],
        } as DeviceEvents,
      ],
    });
  });
});
