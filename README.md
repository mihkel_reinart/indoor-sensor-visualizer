# sensor-visualizer-app

This project is an [Vue.js](https://v3.vuejs.org) application that helps visualize indoor sensor data requested from [Thinnect](www.thinnect.com) API.  
It uses [HTML Canvas](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API) to draw the visualizations   
and has been built using [Typescript](https://www.typescriptlang.org).
[Tailwind css](https://tailwindcss.com/) is used to make things prettier.

## How To

### Environment variables
Create a `.env` file containing the necessary environment variables by
copying `.env.example` and substituting the values.

### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Compiles and minifies for production
```
npm run build
```

#### Runs tests
```
npm run test:unit
```

#### Lints and fixes files
```
npm run lint
```

## Project structure

The project follows a pretty standard Vue project setup, although there are some application specific folders.

    .
    ├── ...
    ├── src/   
    │   ├── canvas          # Code that draws on canvas 
    │   ├── components      # Different self contained UI elements. Data comes in via props and leaves by emitting events
    │   |
    │   |── composables     # Separated parts of code that handle interactions on canvas
    │   |── store           # Temporary persistance layer (in memory) that holds state
    │   |── util
    |   └── views           # Different pages. Combines UI components and passes data from store 
    ├── api.ts              # API requests
    ├── main.ts             # Application entry point 
    ├── router.ts
    ├── tests/ 
    └── ...

It is worth noting that the most important components are the following:

    ├── ...
    ├── src/components   
    │   ├── 
    │   ├── EventSequenceMap.vue        # Combines and coordinates between MapCanvas and EventSequencePlayer components
    │   |
    │   ├── EventSequencePlayer.vue     # Displays a bar containing all current events (each represented as a button)
    │   |                               # Allows play-through of event sequence and skipping to specific event when clicking on corresponding button  
    │   |
    │   ├── MapCanvas.vue               # Component that contains the canvas element used for visualizing data
    │   |                               # Delegates drawing to layers found in src/canvas/ folder
    |   |...

## Authors
Mihkel Reinart

## License
[MIT](LICENSE.md)
