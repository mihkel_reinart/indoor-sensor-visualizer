export default function useAnimationLoop(callback: Function) {
  function animate() {
    callback();
    window.requestAnimationFrame(animate);
  }

  window.requestAnimationFrame(animate);
}
