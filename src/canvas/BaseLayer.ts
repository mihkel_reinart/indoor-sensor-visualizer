import { MapCanvasContext } from '@/types';
import CoordinatesUtil from '@/util/CoordinatesUtil';

export default abstract class BaseLayer {
  protected renderCtx: CanvasRenderingContext2D;

  protected mapCanvasCtx: MapCanvasContext;

  constructor(renderContext: CanvasRenderingContext2D, mapContext: MapCanvasContext) {
    this.renderCtx = renderContext;
    this.mapCanvasCtx = mapContext;
  }

  abstract render(): void;

  convertX(x: number): number {
    return CoordinatesUtil.convertToCanvasXCoordinate(x, this.renderCtx.canvas, this.mapCanvasCtx);
  }

  convertY(y: number): number {
    return CoordinatesUtil.convertToCanvasYCoordinate(y, this.renderCtx.canvas, this.mapCanvasCtx);
  }
}
