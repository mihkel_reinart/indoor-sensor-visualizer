import BaseLayer from '@/canvas/BaseLayer';
import {
  Device,
  DeviceEvents,
  Event,
  EventType,
  MapCanvasContext,
} from '@/types';
import EventTypeScale from '@/canvas/EventTypeScale';

interface Point {
  x: number;
  y: number;
}

interface Rectangle {
  point: Point;
  width: number;
  height: number;
}

export default class EventsLayer extends BaseLayer {

  private _devicesEvents!: DeviceEvents[];

  private _eventTypes!: EventType[];

  private _transitions: Map<string, number>;

  constructor(renderContext: CanvasRenderingContext2D, mapContext: MapCanvasContext) {
    super(renderContext, mapContext);
    this._transitions = new Map<string, number>();
  }

  render(): void {
    if (this._devicesEvents.length === 0) {
      const x = this.convertX(0);
      const y = this.convertY(0);
      this.renderDeviceName('No events to display.', { x, y });
      this.renderDeviceName('Use filters to fetch events.', { x, y: y + 20 });
      return;
    }

    this._devicesEvents.forEach((deviceEvent: DeviceEvents) => {
      const { device } = deviceEvent;
      const { events } = deviceEvent;

      const x = this.convertX(device.localLatitude);
      const y = this.convertY(device.localLongitude);
      const p = { x, y } as Point;

      events.forEach((event, index) => {
        if (events.length === 1) {
          this.renderEventHeatmap(device, event, p);
        } else {
          this.renderEventColorBar(device, event, p, index);
        }
      });

      this.renderDeviceLocationCircle(p);
      this.renderDeviceName(device.name, p);
    });
  }

  setDevicesEvents(deviceEvents: DeviceEvents[]): void {
    this._devicesEvents = deviceEvents;
  }

  setEventTypes(eventTypes: EventType[]): void {
    this._eventTypes = eventTypes;
  }

  private renderDeviceName(name: string, p: Point): void {
    const ctx = this.renderCtx;
    const { x, y } = p;

    ctx.save();
    ctx.font = `${20 - this.mapCanvasCtx.zoom}px Helvetica`;
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 2;
    ctx.fillStyle = '#fff';
    ctx.textAlign = 'center';
    ctx.strokeText(`${name}`, x, y - 15);
    ctx.fillText(`${name}`, x, y - 15);
    ctx.restore();
  }

  private renderDeviceLocationCircle(p: Point): void {
    const ctx = this.renderCtx;
    const { x, y } = p;

    ctx.save();
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 2;
    ctx.fillStyle = '#fff';
    ctx.beginPath();
    ctx.arc(x, y, 4, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.globalAlpha = 0.6;
    ctx.fill();
    ctx.restore();
  }

  private renderEventHeatmap(device: Device, event: Event, p: Point): void {
    this.renderHeatmapCircle(device, event, p);
    this.renderHeatmapTexts(event, p);
  }

  private renderHeatmapCircle(device: Device, event: Event, p: Point): void {
    const ctx = this.renderCtx;
    const { x, y } = p;

    const value = this.getTransitionValue(`${device.id}_${event.eventType}`, event.value);
    const percent = EventsLayer.getCurrentValuePercent(event.eventType, value);

    const circleSize = 35;
    ctx.save();
    ctx.fillStyle = EventTypeScale.getColor(event.eventType, percent);
    ctx.filter = `blur(${circleSize / 2}px)`;
    ctx.beginPath();
    ctx.arc(x, y, circleSize, 0, 2 * Math.PI);
    ctx.fill();
    ctx.restore();
  }

  private renderHeatmapTexts(event: Event, p: Point) {
    const ctx = this.renderCtx;
    const { x, y } = p;
    const eventType = this.getEventType(event.eventType);

    ctx.save();
    ctx.font = '12px Helvetica';
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 2;
    ctx.fillStyle = '#fff';
    ctx.textAlign = 'center';
    ctx.strokeText(`${eventType.name} ${event.value} ${eventType.unit}`, x, y + 20);
    ctx.fillText(`${eventType.name} ${event.value} ${eventType.unit}`, x, y + 20);
    ctx.restore();
  }

  private renderEventColorBar(device: Device, event: Event, p: Point, offset: number): void {
    const barWidth = 100;
    const rectangle = {
      point: {
        x: p.x - (barWidth / 2),
        y: p.y + 10 + (offset * 20),
      } as Point,
      width: 100,
      height: 10,
    } as Rectangle;

    this.renderColorBarBackground(event, rectangle);
    this.renderColorBarTexts(event, rectangle);
    this.renderColorBarBorder(rectangle);
    this.renderColorBarIndicator(device, event, rectangle);
  }

  private renderColorBarIndicator(device: Device, event: Event, r: Rectangle): void {
    const ctx = this.renderCtx;

    const value = this.getTransitionValue(`${device.id}_${event.eventType}`, event.value);
    const percent = EventsLayer.getCurrentValuePercent(event.eventType, value);
    const indicatorX = r.point.x + (r.width * (percent / 100));

    const indicatorWidth = 2;
    const indicatorOverflowY = 5;

    const x = indicatorX - (indicatorWidth / 2);
    const y = r.point.y - indicatorOverflowY;
    const w = indicatorWidth;
    const h = r.height + (indicatorOverflowY * 2);

    ctx.save();
    ctx.beginPath();
    ctx.fillStyle = '#000';
    ctx.strokeStyle = '#fff';
    ctx.rect(x, y, w, h);
    ctx.fill();
    ctx.stroke();
    ctx.restore();
  }

  private renderColorBarBorder(r: Rectangle): void {
    const ctx = this.renderCtx;
    const thickness = 1;
    const x = r.point.x - thickness;
    const y = r.point.y - thickness;
    const w = r.width + (thickness * 2);
    const h = r.height + (thickness * 2);

    ctx.save();
    ctx.strokeStyle = '#000';
    ctx.strokeRect(x, y, w, h);
    ctx.restore();
  }

  private renderColorBarTexts(event: Event, r: Rectangle): void {
    const ctx = this.renderCtx;
    const eventType = this.getEventType(event.eventType);
    const leftX = r.point.x - 5;
    const rightX = r.point.x + r.width + 5;
    const textPosY = r.point.y + r.height;

    ctx.save();
    ctx.font = '12px Helvetica';
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 2;
    ctx.fillStyle = '#fff';

    ctx.textAlign = 'right';
    ctx.strokeText(`${eventType.name}`, leftX, textPosY);
    ctx.fillText(`${eventType.name}`, leftX, textPosY);

    ctx.textAlign = 'left';
    ctx.strokeText(`${eventType.unit}`, rightX, textPosY);
    ctx.fillText(`${eventType.unit}`, rightX, textPosY);
    ctx.restore();
  }

  private renderColorBarBackground(event: Event, r: Rectangle) {
    const ctx = this.renderCtx;
    const { width, height, point } = r;

    const colorScale = {
      0: EventTypeScale.getColor(event.eventType, 0),
      1: EventTypeScale.getColor(event.eventType, 25),
      2: EventTypeScale.getColor(event.eventType, 50),
      3: EventTypeScale.getColor(event.eventType, 75),
      4: EventTypeScale.getColor(event.eventType, 100),
    };

    const gradient = ctx.createLinearGradient(point.x, 0, point.x + width, 0);
    const numberOfColors = Object.values(colorScale).length;
    Object.entries(colorScale).forEach(([key, hexColor]) => {
      gradient.addColorStop(Number(key) / numberOfColors, hexColor);
    });
    ctx.fillStyle = gradient;

    ctx.save();
    ctx.beginPath();
    ctx.rect(point.x, point.y, width, height);
    ctx.fill();
    ctx.restore();
  }

  private static getCurrentValuePercent(eventType: string, value: number): number {
    const scaleMin = EventTypeScale.getScaleStart(eventType);
    const scaleMax = EventTypeScale.getScaleEnd(eventType);
    const scaleWidth = scaleMax - scaleMin;
    const scaleValue = Math.min(Math.max(value, scaleMin), scaleMax);

    return 100 * Math.max(
      0,
      Math.min((scaleValue - scaleMin) / scaleWidth, 1),
    );
  }

  private getEventType(eventTypeId: string): EventType {
    const eventType = this._eventTypes.find(
      (eventType: EventType) => eventType.id === eventTypeId,
    );
    return eventType || { name: '', unit: '' } as EventType;
  }

  private getTransitionValue(key: string, value: number): number {
    if (this._transitions.has(key)) {
      let transitioningValue = value;
      const prevValue = this._transitions.get(key) || value;
      if (prevValue !== value) {
        const delta = prevValue - value;

        //do not let the deltas become too big to achieve smoother animation
        const deltaStep = Math.max(0.25, Math.abs(delta * 0.075));

        if (delta > 0) {
          transitioningValue = Math.max(prevValue - deltaStep, value);

        } else if (delta < 0) {
          transitioningValue = Math.min(prevValue + deltaStep, value);

        }
      }
      this._transitions.set(key, transitioningValue);

    } else {
      this._transitions.set(key, value);

    }

    return this._transitions.get(key) || 0;
  }
}
