import { mount } from '@vue/test-utils';
import MapCanvas from '@/components/MapCanvas.vue';

describe('map canvas', () => {
  const getMockComponent = () => {
    //we do not care about testing the canvas drawing here
    HTMLCanvasElement.prototype.getContext = () => null;
    return mount(MapCanvas);
  };

  it('renders a canvas tag', async () => {
    const wrapper = getMockComponent();
    expect(wrapper.find('canvas').exists()).toBeTruthy();
  });

  it('renders zooming buttons', async () => {
    const wrapper = getMockComponent();
    const zoomInButton = wrapper.find('[data-testid="zoomInButton"]');
    const zoomOutButton = wrapper.find('[data-testid="zoomOutButton"]');

    expect(zoomInButton.exists()).toBeTruthy();
    expect(zoomOutButton.exists()).toBeTruthy();
  });
});
